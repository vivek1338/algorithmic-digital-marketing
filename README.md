# Assignment 1
Report on Rakuten
  Analyse the website and identify the various the algorithms used for pricing, promotion, search and recommendation.

# Assignment 2
Dashboard for Marketa Analytics
 Using Einstein Analytics, build a visualization dashboard for the client to understand impact of the various attributes in pricing and promotion and build a recommendation system.
 
# Assignment 3
Analysis Dashboard using Salesforce Einstien Analytics
   Understand the effect of adstock marketting strategy on the sales over a given period of time and develop a dashboard to demonstrate the impact of tv ads versus the radio ads.
